package com.disney.datg.eia.devops.platform.sample.jar;

import lombok.Getter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

@Getter
public class Parameters {
    @QueryParam("offset")
    @DefaultValue("0")
    @Min(0)
    private Integer offset;

    @QueryParam("limit")
    @DefaultValue("20")
    @Max(100)
    @Min(1)
    private Integer limit;
}