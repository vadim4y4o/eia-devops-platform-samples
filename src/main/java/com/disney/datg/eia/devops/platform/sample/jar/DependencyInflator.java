package com.disney.datg.eia.devops.platform.sample.jar;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Map;

import javax.el.ArrayELResolver;
import javax.naming.ldap.LdapName;

import org.apache.commons.collections.bidimap.DualTreeBidiMap;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.coyote.http11.filters.ChunkedInputFilter;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.utils.CloseableUtils;
import org.apache.cxf.jaxrs.JAXRSBinding;
import org.apache.cxf.jaxrs.client.LocalClientState;
import org.apache.cxf.rs.security.cors.CorsHeaderConstants;
import org.apache.cxf.transport.servlet.BaseUrlHelper;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.nio.client.util.HttpAsyncClientUtils;
import org.apache.zookeeper.ZooKeeper;
import org.ehcache.core.Ehcache;
import org.ehcache.xml.XmlConfiguration;
import org.hibernate.validator.constraintvalidators.RegexpURLValidator;
import org.jboss.logging.NDC;
import org.joda.time.DateTimeZone;
import org.springframework.boot.actuate.metrics.Metric;
import org.springframework.cloud.consul.discovery.ConsulCatalogWatch;
import org.springframework.data.redis.core.TimeoutUtils;
import org.springframework.ldap.support.LdapUtils;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.validation.DataBinder;
import org.springframework.web.context.request.async.DeferredResult;

import com.eaio.uuid.UUID;
import com.ecwid.consul.transport.RawResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.jaxrs.base.JsonMappingExceptionMapper;
import com.fasterxml.jackson.jaxrs.json.PackageVersion;
import com.google.common.escape.Escaper;
import com.google.common.xml.XmlEscapers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.protobuf.UnknownFieldSet;
import com.google.protobuf.UnknownFieldSet.Builder;
import com.sun.el.ValueExpressionLiteral;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import bsh.XThis;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.net.SocketNode;
import io.jsonwebtoken.impl.DefaultClock;
import lombok.Getter;
import redis.clients.jedis.BinaryJedis;

public class DependencyInflator {

	public void dependencyCaller() {
		// Spring
		DeferredResult<String> defResult = new DeferredResult<String>();
		defResult.toString();
		DataBinder binder = new DataBinder(null);	
		binder.toString();
		ConsulCatalogWatch catalogWatch = new ConsulCatalogWatch(null, null);
		catalogWatch.toString();
		TimeoutUtils.toMillis(0, null);
		SpringSecurityMessageSource messageSource = new SpringSecurityMessageSource();
		messageSource.toString();
		LdapName ldapName = LdapUtils.emptyLdapName();
		ldapName.toString();
		Metric<Integer> metric = new Metric<Integer>(null, null);
		metric.toString();
		
		//Jedis
		BinaryJedis jedis = new BinaryJedis();
		jedis.toString();
		jedis.close();

		// ZooKeeper
		try {
			ZooKeeper zk = new ZooKeeper(null, 0, null);
			zk.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// Jackson-core
		TypeReference<Map<String, String>> typeRef = new TypeReference<Map<String, String>>() {
		};
		typeRef.toString();

		// Jackson-databind
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.toString();

		// Jackson-jaxrs-base
		JsonMappingExceptionMapper exceptionMapper = new JsonMappingExceptionMapper();
		exceptionMapper.toString();

		// Jackson-jaxrs-json-provider
		PackageVersion pkgVersion = new PackageVersion();
		pkgVersion.toString();

		// Jackson-dataformat-yaml
		YAMLMapper yamlMapper = new YAMLMapper();
		yamlMapper.toString();

		// Jackson-datatype-joda
		JodaModule jodaMod = new JodaModule();
		jodaMod.toString();

		// Commons-lang3
		Double dbl = NumberUtils.DOUBLE_MINUS_ONE;
		dbl.toString();

		// Gson
		Gson gson = new GsonBuilder().create();
		gson.toString();

		// ehcache
		Ehcache<Object, Object> ehcache = new Ehcache<Object, Object>(null, null, null, null);
		ehcache.close();

		// consul-api
		RawResponse rawResponse = new RawResponse(0, null, null, null, null, null);
		rawResponse.toString();

		// Protobuf-java
		Builder unknownFields = UnknownFieldSet.newBuilder();
		unknownFields.clear();

		// logback-classic
		Level lvl = Level.ERROR;
		lvl.toString();

		// UUID
		UUID uuid = new UUID();
		uuid.toString();

		// Guava
		Escaper escapers = XmlEscapers.xmlAttributeEscaper();
		escapers.toString();

		// Commons-collection
		DualTreeBidiMap treeMap = new DualTreeBidiMap();
		treeMap.toString();

		// Commons-configuration
		CompositeConfiguration config = new CompositeConfiguration();
		config.toString();

		// jjwt
		DefaultClock clock = new DefaultClock();
		clock.toString();

		// javax.el-api
		ArrayELResolver elResolver = new ArrayELResolver();
		elResolver.toString();
		
		//javax.el
		ValueExpressionLiteral expressionLiteral = new ValueExpressionLiteral();
		expressionLiteral.toString();

		// joda-time
		DateTimeZone dateTimeZone = DateTimeZone.UTC;
		dateTimeZone.toString();
		
		//jcache
		XmlConfiguration xmlConfig = new XmlConfiguration(null);
		xmlConfig.toString();
		
		//Apache CXF
		JAXRSBinding binding = new JAXRSBinding(null);
		binding.toString();
		LocalClientState clientState = new LocalClientState();
		clientState.toString();
		String corsConstant = CorsHeaderConstants.HEADER_AC_ALLOW_CREDENTIALS;
		corsConstant.toString();
		
		//Apache httpcomponents
		HttpAsyncClientUtils.closeQuietly(null);
		FileBody fileBody = new FileBody(null);
		fileBody.toString();
		
		//Apache curator
		ConnectionState connState = ConnectionState.CONNECTED;
		connState.toString();
		CloseableUtils.closeQuietly(null);
		
		//Hibernate validator
		RegexpURLValidator urlValidator = new RegexpURLValidator();
		urlValidator.toString();
		
		//jboss-logging
		String ndc = NDC.pop();
		ndc.toString();
		
		//Archaius
		try {
			URL url = ConfigurationUtils.getURL(null, null);
			url.toString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	// Lombok
	// Jackson-annotation
	@Getter
	@ApiModel("Authentication")
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonIgnoreProperties(ignoreUnknown = true)
	public class Authentication {

		@ApiModelProperty(value = "Placeholder Property", readOnly = true)
		private String type;

		@ApiModelProperty(value = "Placeholder Value", readOnly = true)
		private Collection<String> values;
	}
	
	public void clmThreatAdder() {
		//tomcat-coyote
		ChunkedInputFilter inputFilter = new ChunkedInputFilter(0, 0);
		inputFilter.toString();
		
		//cxf-rt-transports-http
		String urlHelper = BaseUrlHelper.getBaseURL(null);
		urlHelper.toString();
		
		//logback-classic
		SocketNode socketNode = new SocketNode(null, null, null);
		socketNode.toString();
		
		//beanshell
		XThis xthis = new XThis(null, null);
		xthis.toString();
	}
}
