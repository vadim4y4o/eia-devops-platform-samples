package com.disney.datg.eia.devops.platform.sample.jar;

import io.swagger.annotations.*;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

@Path("/store")
@Api(value="/store", tags = "store")
@Produces({"application/json", "application/xml"})
public class PetStoreResource {
  //static StoreData storeData = new StoreData();
  //static PetData petData = new PetData();

  @GET
  @Path("/inventory")
  @Produces({MediaType.APPLICATION_JSON})
  @ApiOperation(value = "Returns pet inventories by status", 
    notes = "Returns a map of status codes to quantities", 
    response = Integer.class,
    responseContainer = "map",
    authorizations = @Authorization(value = "api_key")
  )
  public java.util.Map<String, Integer> getInventory() {
    //return petData.getInventoryByStatus();
	  return null;
  }

  @GET
  @Path("/order/{orderId}")
  @ApiOperation(value = "Find purchase order by ID",
    notes = "For valid response try integer IDs with value >= 1 and <= 10. Other values will generated exceptions",
    response = Order.class)
  @ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid ID supplied"),
      @ApiResponse(code = 404, message = "Order not found") })
  public Response getOrderById(
      @ApiParam(value = "ID of pet that needs to be fetched", allowableValues = "range[1,10]", required = true)
      @PathParam("orderId") Long orderId)
      throws NotFoundException {
    //Order order = storeData.findOrderById(orderId);
    
    
    return null;
  }

  @POST
  @Path("/order")
  @ApiOperation(value = "Place an order for a pet")
  @ApiResponses({ @ApiResponse(code = 400, message = "Invalid Order") })
  public Order placeOrder(
      @ApiParam(value = "order placed for purchasing the pet",
        required = true) Order order) {
    
    return null;
  }

  @DELETE
  @Path("/order/{orderId}")
  @ApiOperation(value = "Delete purchase order by ID",
    notes = "For valid response try integer IDs with positive integer value. Negative or non-integer values will generate API errors")
  @ApiResponses(value = { @ApiResponse(code = 400, message = "Invalid ID supplied"),
      @ApiResponse(code = 404, message = "Order not found") })
  public Response deleteOrder(
      @ApiParam(value = "ID of the order that needs to be deleted", allowableValues = "range[1,infinity]", required = true)
      @PathParam("orderId") Long orderId) {
    
	  return null;
  }
  
  @GET
  @ApiOperation(value = "Get order2",
      notes = "Order2")
  @ApiResponses({
      @ApiResponse(code = 200,
          response = Order.class,
          message = "OKAY MESSAGE HERE"),
      @ApiResponse(code = 400,
          response = Order.class,
          message = "Nope"),
      @ApiResponse(code = 500,
          message = "Nope")
  })
  public Order getOrder2(@BeanParam Parameters parameters, @BeanParam Parameters paginationParameters) {
	return null;
  }
}