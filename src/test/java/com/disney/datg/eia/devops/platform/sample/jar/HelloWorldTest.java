package com.disney.datg.eia.devops.platform.sample.jar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HelloWorldTest {

	@Test
	public void test_getDisplayAccesslevels() {
		HelloWorld hello = new HelloWorld();
		assertEquals(hello.getHello(), "Hello, World!");
	}

}

